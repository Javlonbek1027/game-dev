package uz.itic.itic_company.example.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.itic.itic_company.example.common.baseservice.Deletable;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.CustomPage;
import uz.itic.itic_company.example.model.dto.GamesDTO;
import uz.itic.itic_company.example.model.dto.GamesDTOForCommon;
import uz.itic.itic_company.example.model.dto.req.PaginationRequest;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.entity.Games;
import uz.itic.itic_company.example.model.enums.ETypeGames;
import uz.itic.itic_company.example.model.mapper.GamesMapper;
import uz.itic.itic_company.example.projection.GamesProjection;
import uz.itic.itic_company.example.repository.GamesRepository;

import java.util.List;
import java.util.UUID;

@Service
public class GamesService extends GenericService<Games, UUID, GamesDTO, GamesRepository, GamesMapper> implements Deletable {
    public GamesService(GamesRepository repository, GamesMapper mapper) {
        super(repository, mapper, Games.class);
    }

    public CustomPage<GamesDTOForCommon> getActiveGames(PaginationRequest paginationRequest,String type,String lan) {
        Page<GamesProjection> allByIsActiveTrue = repository.findAllByIsActiveTrue(type,paginationRequest.getPageRequestForNativeQuery());
        List<GamesDTOForCommon> gamesDTOs = allByIsActiveTrue.getContent().stream()
                .map((GamesProjection gamesProjection) -> mapToBlogDTO(gamesProjection,lan)).toList();
        return CustomPage.of(allByIsActiveTrue, gamesDTOs);
    }

    private static GamesDTOForCommon mapToBlogDTO(GamesProjection gamesProjection,String lan) {
        GamesDTOForCommon gamesDto = new GamesDTOForCommon();
        gamesDto.setId(gamesProjection.getId());
        gamesDto.setType(ETypeGames.valueOf(gamesProjection.getType()));
        gamesDto.setGamePhotoUrl(gamesProjection.getBlogPhotoUrl());
        gamesDto.setVideoUrl(gamesProjection.getVideoUrl());
        gamesDto.setIsActive(gamesProjection.getIsActive());
        gamesDto.setCreatedAt(gamesProjection.getCreatedAt());
        switch (lan) {
            case "EN" -> {
                gamesDto.setTitle(gamesProjection.getTitleEn());
                gamesDto.setContent(gamesProjection.getContentEn());
            }
            case "UZ" -> {
                gamesDto.setTitle(gamesProjection.getTitleUz());
                gamesDto.setContent(gamesProjection.getContentUz());
            }
            case "RU" -> {
                gamesDto.setTitle(gamesProjection.getTitleRu());
                gamesDto.setContent(gamesProjection.getContentRu());
            }
        }
        return gamesDto;
    }

}
