package uz.itic.itic_company.example.service;

import org.springframework.stereotype.Service;
import uz.itic.itic_company.example.common.baseservice.Deletable;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.TeamDataDTO;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.entity.TeamData;
import uz.itic.itic_company.example.model.mapper.TeamDataMapper;
import uz.itic.itic_company.example.repository.TeamDataRepository;

import java.util.UUID;

@Service
public class TeamDataService extends GenericService<TeamData, UUID, TeamDataDTO, TeamDataRepository, TeamDataMapper> implements Deletable {
    public TeamDataService(TeamDataRepository repository, TeamDataMapper mapper) {
        super(repository, mapper, TeamData.class);
    }

    @Override
    public TeamDataDTO create(TeamDataDTO dto) {
        TeamDataDTO teamDataDTO =new TeamDataDTO();
        if (repository.findAll().isEmpty()){
            teamDataDTO= mapper.toDto(repository.save(mapper.toEntity(dto)));
        }else {
            for (TeamData teamData : repository.findAll()) {
                teamData.setCustomers(dto.getCustomers());
                teamData.setOffices(dto.getOffices());
                teamData.setEmployees(dto.getEmployees());
                teamData.setProjects(dto.getProjects());
                teamDataDTO = mapper.toDto(teamData);
            }

        }
        return teamDataDTO;
    }
}
