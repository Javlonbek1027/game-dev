package uz.itic.itic_company.example.service;

import org.springframework.stereotype.Service;
import uz.itic.itic_company.example.common.baseservice.Deletable;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.LogoDto;
import uz.itic.itic_company.example.model.entity.LogoEntity;
import uz.itic.itic_company.example.model.mapper.LogoMapper;
import uz.itic.itic_company.example.repository.LogoRepository;

import java.util.UUID;

@Service
public class LogoService extends GenericService<LogoEntity, UUID, LogoDto, LogoRepository, LogoMapper> implements Deletable {
    public LogoService(LogoRepository repository, LogoMapper mapper, LogoRepository logoRepository) {
        super(repository, mapper, LogoEntity.class);
    }

    public LogoDto getFirst() {
        return mapper.toDto(repository.getTopByCreatedAtIsNotNull());
    }
}
