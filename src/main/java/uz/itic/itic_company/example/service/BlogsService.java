package uz.itic.itic_company.example.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.itic.itic_company.example.common.baseservice.Deletable;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.BlogsDTO;
import uz.itic.itic_company.example.model.dto.BlogsDTOForCommon;
import uz.itic.itic_company.example.model.dto.CustomPage;
import uz.itic.itic_company.example.model.dto.req.PaginationRequest;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.mapper.BlogsMapper;
import uz.itic.itic_company.example.projection.BlogProjection;
import uz.itic.itic_company.example.repository.BlogsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class BlogsService extends GenericService<Blogs, UUID, BlogsDTO, BlogsRepository, BlogsMapper> implements Deletable {
    private final BlogsRepository blogsRepository;

    public BlogsService(BlogsRepository repository, BlogsMapper mapper, BlogsRepository blogsRepository) {
        super(repository, mapper, Blogs.class);
        this.blogsRepository = blogsRepository;
    }

    public CustomPage<BlogsDTOForCommon> getActiveBlogs(PaginationRequest paginationRequest,String lan) {
        Page<BlogProjection> allByIsActiveTrue = blogsRepository.findAllByIsActiveTrue(paginationRequest.getPageRequestForNativeQuery());
        List<BlogsDTOForCommon> blogDTOs = allByIsActiveTrue.getContent().stream()
                .map((BlogProjection blogProjection) -> mapToBlogDTO(blogProjection,lan)).toList();
        return CustomPage.of(allByIsActiveTrue, blogDTOs);
    }

    private BlogsDTOForCommon mapToBlogDTO(BlogProjection blogProjection, String lan) {
        BlogsDTOForCommon blogDto = new BlogsDTOForCommon();
        blogDto.setBlogPhotoUrl(blogProjection.getBlogPhotoUrl());
        blogDto.setId(blogProjection.getId());
        blogDto.setVideoUrl(blogProjection.getVideoUrl());
        blogDto.setIsActive(blogProjection.getIsActive());
        blogDto.setCreatedAt(blogProjection.getCreatedAt());
        switch (lan){
            case "EN" ->{
                blogDto.setTitle(blogProjection.getTitleEn());
                blogDto.setContent(blogProjection.getContentEn());
            }
            case "RU" ->{
                blogDto.setTitle(blogProjection.getTitleRu());
                blogDto.setContent(blogProjection.getContentRu());
            }
            case "UZ"->{
                blogDto.setTitle(blogProjection.getTitleUz());
                blogDto.setContent(blogProjection.getContentUz());
            }
        }

        return blogDto;
    }
}

