package uz.itic.itic_company.example.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.entity.Games;
import uz.itic.itic_company.example.projection.BlogProjection;
import uz.itic.itic_company.example.projection.GamesProjection;
import uz.itic.itic_company.example.repository.base.BaseRepository;

import java.util.UUID;

@Repository
public interface GamesRepository extends BaseRepository<Games, UUID> {

    @Query(value = """
             select b.id as id,
                   b.title_uz as titleUz,
                   b.content_uz as contentUz,
                   b.title_ru as titleRu,
                   b.content_ru as contentRu,
                   b.title_en as titleEn,
                   b.content_en as contentEn,
                   b.is_active as isActive,
                   b.blog_photo_url as blogPhotoUrl,
                   b.video_url as videoUrl,
                   b.created_at as createdAt,
                   b.type_of_game as type
            from games b where  b.is_active=true and b.type_of_game = ?1
            """, nativeQuery = true)
    Page<GamesProjection> findAllByIsActiveTrue(String type, Pageable pageable);
}
