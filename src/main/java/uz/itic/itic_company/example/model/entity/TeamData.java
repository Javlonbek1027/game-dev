package uz.itic.itic_company.example.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.*;
import uz.itic.itic_company.example.model.entity.base.BaseEntityUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "team_data")
@Builder
public class TeamData extends BaseEntityUID {
    @Column
    private Integer employees;
    @Column
    private Integer projects;
    @Column
    private Integer customers;
    @Column
    private Integer offices;

}
