package uz.itic.itic_company.example.model.mapper;

import org.mapstruct.Mapper;
import uz.itic.itic_company.example.model.dto.TeamDataDTO;
import uz.itic.itic_company.example.model.entity.TeamData;
import uz.itic.itic_company.example.model.mapper.base.GenericMapper;

@Mapper(componentModel = "spring")
public interface TeamDataMapper extends GenericMapper<TeamData, TeamDataDTO> {
}
