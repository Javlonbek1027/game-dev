package uz.itic.itic_company.example.model.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.itic.itic_company.example.model.dto.base.BaseDTO;

import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TeamDataDTO extends BaseDTO<UUID> {
    private Integer employees;
    private Integer projects;
    private Integer customers;
    private Integer offices;

}
