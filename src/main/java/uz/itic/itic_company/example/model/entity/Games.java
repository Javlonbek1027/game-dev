package uz.itic.itic_company.example.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import uz.itic.itic_company.example.model.entity.base.BaseEntityUID;
import uz.itic.itic_company.example.model.enums.ETypeGames;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "games")
@Builder
public class Games extends BaseEntityUID {
    @Column(length = 1000,name = "title_uz")
    private String titleUz;

    @Column(columnDefinition = "TEXT",name = "content_uz")
    private String contentUz;
    @Column(length = 1000,name = "title_ru")
    private String titleRu;

    @Column(columnDefinition = "TEXT",name = "content_ru")
    private String contentRu;
    @Column(length = 1000,name = "title_en")
    private String titleEn;

    @Column(columnDefinition = "TEXT",name = "content_en")
    private String contentEn;

    @Column(length = 1024,name = "blog_photo_url")
    private String gamePhotoUrl;

    @Column(name = "video_url", length = 1024)
    private String videoUrl;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "type_of_game")
    private ETypeGames typeOfGame;

    @Column(name = "is_active")
    private Boolean isActive;

}
