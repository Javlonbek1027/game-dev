package uz.itic.itic_company.example.model.mapper;

import org.mapstruct.Mapper;
import uz.itic.itic_company.example.model.dto.BlogsDTO;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.mapper.base.GenericMapper;
@Mapper(componentModel = "spring")
public interface BlogsMapper extends GenericMapper<Blogs, BlogsDTO> {
}
