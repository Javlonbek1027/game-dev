package uz.itic.itic_company.example.model.entity;

import jakarta.persistence.*;
import lombok.*;
import uz.itic.itic_company.example.model.entity.base.BaseEntityUID;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "blogs")
@Builder
public class Blogs extends BaseEntityUID {
    @Column(length = 1000,name = "title_uz")
    private String titleUz;

    @Column(columnDefinition = "TEXT",name = "content_uz")
    private String contentUz;
    @Column(length = 1000,name = "title_ru")
    private String titleRu;

    @Column(columnDefinition = "TEXT",name = "content_ru")
    private String contentRu;
    @Column(length = 1000,name = "title_en")
    private String titleEn;

    @Column(columnDefinition = "TEXT",name = "content_en")
    private String contentEn;

    @Column(length = 1024,name = "blog_photo_url")
    private String blogPhotoUrl;

    @Column(name = "video_url", length = 1024)
    private String videoUrl;

    @Column(name = "is_active")
    private Boolean isActive;

}
