package uz.itic.itic_company.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.itic.itic_company.example.model.dto.base.BaseDTO;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlogsDTOForCommon extends BaseDTO<UUID> {
    private String title;
    private String content;
    private String blogPhotoUrl;
    private String videoUrl;
    private Boolean isActive;
}
