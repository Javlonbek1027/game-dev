package uz.itic.itic_company.example.model.mapper;

import org.mapstruct.Mapper;
import uz.itic.itic_company.example.model.dto.GamesDTO;
import uz.itic.itic_company.example.model.entity.Games;
import uz.itic.itic_company.example.model.mapper.base.GenericMapper;

@Mapper(componentModel = "spring")
public interface GamesMapper extends GenericMapper<Games, GamesDTO> {
}
