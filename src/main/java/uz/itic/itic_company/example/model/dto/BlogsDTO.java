package uz.itic.itic_company.example.model.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.itic.itic_company.example.model.dto.base.BaseDTO;

import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlogsDTO extends BaseDTO<UUID> {
    private String titleUz;
    private String contentUz;
    private String titleRu;
    private String contentRu;
    private String titleEn;
    private String contentEn;
    private String blogPhotoUrl;
    private String videoUrl;
    private Boolean isActive;
}
