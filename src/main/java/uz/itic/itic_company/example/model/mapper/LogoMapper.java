package uz.itic.itic_company.example.model.mapper;

import org.mapstruct.Mapper;
import uz.itic.itic_company.example.model.dto.LogoDto;
import uz.itic.itic_company.example.model.entity.LogoEntity;
import uz.itic.itic_company.example.model.mapper.base.GenericMapper;

@Mapper(componentModel = "spring", uses = AttachmentMapper.class)
public interface LogoMapper extends GenericMapper<LogoEntity, LogoDto> {
}
