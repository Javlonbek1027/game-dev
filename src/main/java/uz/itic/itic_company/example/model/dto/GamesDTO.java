package uz.itic.itic_company.example.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.itic.itic_company.example.model.dto.base.BaseDTO;
import uz.itic.itic_company.example.model.enums.ETypeGames;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GamesDTO extends BaseDTO<UUID> {
    private String titleUz;
    private String contentUz;
    private String titleRu;
    private String contentRu;
    private String titleEn;
    private String contentEn;
    private ETypeGames typeOfGame;
    private String gamePhotoUrl;
    private String videoUrl;
    private Boolean isActive;
}
