package uz.itic.itic_company.example.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.itic.itic_company.example.model.entity.base.BaseEntityUID;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "logo_entity")
@AllArgsConstructor
@NoArgsConstructor
public class LogoEntity extends BaseEntityUID {

    @Column(length = 1024)
    private String logoPhotoUrl;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "attachment_id", insertable = false, updatable = false)
    private Attachment attachment;

    @Column(name = "attachment_id")
    private UUID attachmentId;

}
