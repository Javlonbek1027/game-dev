package uz.itic.itic_company.example.projection;

import java.sql.Timestamp;
import java.util.UUID;

public interface GamesProjection {
    UUID getId();
    String getTitleUz();
    String getContentUz();
    String getTitleRu();
    String getContentRu();
    String getTitleEn();
    String getContentEn();
    String getBlogPhotoUrl();
    String getVideoUrl();
    Boolean getIsActive();

    Timestamp getCreatedAt();
    String getType();
}
