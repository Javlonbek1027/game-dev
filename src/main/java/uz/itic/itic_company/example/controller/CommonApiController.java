package uz.itic.itic_company.example.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.itic.itic_company.example.model.dto.*;
import uz.itic.itic_company.example.model.dto.req.PaginationRequest;
import uz.itic.itic_company.example.model.dto.res.FileCreatedResponse;
import uz.itic.itic_company.example.service.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value =  "/api/public")
@RequiredArgsConstructor
public class CommonApiController {

    private final CallRequestService callRequestService;
    private final FileService fileService;
    private final BlogsService blogService;
    private final GamesService gamesService;
    private final TeamDataService teamDataService;
    private final PartnersService partnersService;
    private final LogoService logoService;

    @PostMapping(value = "/upload")
    public Header<FileCreatedResponse> upload(@RequestParam("file") MultipartFile file) throws IOException {
        return Header.ok(fileService.uploadFile(file));

    }

    @GetMapping(value = "/download/{fileName}")
    public void download(@PathVariable String fileName, HttpServletResponse httpServletResponse) throws IOException {
        fileService.downloadFile(httpServletResponse,fileName);
    }
    @PostMapping("/call-request")
    public Header<CallRequestDto>  sendCallRequest(@RequestBody Header<CallRequestDto> dto){
        return Header.ok(callRequestService.create(dto.getData()));
    }
    @GetMapping(value = "/blogs")
    public Header<List<BlogsDTOForCommon>> getBlogs(PaginationRequest paginationRequest, @RequestParam String lan) {
        var paginated = blogService.getActiveBlogs(paginationRequest,lan);
        return Header.ok(paginated.getData(), paginated.getPaginationData());
    }
    @GetMapping(value = "/games")
    public Header<List<GamesDTOForCommon>> getGames(PaginationRequest paginationRequest,@RequestParam String type,@RequestParam String lan) {
        var paginated = gamesService.getActiveGames(paginationRequest,type,lan);
        return Header.ok(paginated.getData(), paginated.getPaginationData());
    }
    @GetMapping(value = "/team-data")
    public Header<List<TeamDataDTO>>getTeamData(){
        return Header.ok(teamDataService.getList());
    }
    @GetMapping(value = "/partners")
    public Header<List<PartnerDto>>getPartners(){
        return Header.ok(partnersService.getList());
    }
    @GetMapping(value = "/logo")
    public Header<LogoDto> getLogo(){
        return Header.ok(logoService.getFirst());
    }
}
