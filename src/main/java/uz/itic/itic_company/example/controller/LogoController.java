package uz.itic.itic_company.example.controller;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.itic.itic_company.example.common.basecontroller.GenericController;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.Header;
import uz.itic.itic_company.example.model.dto.LogoDto;
import uz.itic.itic_company.example.model.entity.LogoEntity;
import uz.itic.itic_company.example.model.mapper.LogoMapper;
import uz.itic.itic_company.example.repository.LogoRepository;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/admin/logo")
public class LogoController extends GenericController<LogoEntity, UUID, LogoDto, LogoRepository, LogoMapper> {

    private final LogoRepository logoRepository;
    private final LogoMapper logoMapper;

    public LogoController(GenericService<LogoEntity, UUID, LogoDto, LogoRepository, LogoMapper> service,
                          LogoRepository logoRepository, LogoMapper logoMapper) {
        super(service);
        this.logoRepository = logoRepository;
        this.logoMapper = logoMapper;
    }

//    @PostMapping
//    public Header<LogoDto> create(Header<LogoDto> logoDto) {
//        if(logoRepository.findAll().isEmpty()){
//            return super.create(logoDto);
//        }
//        throw new BadCredentialsException("don't have access!");
//    }

//test
}
