package uz.itic.itic_company.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.itic.itic_company.example.common.basecontroller.GenericController;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.BlogsDTO;
import uz.itic.itic_company.example.model.entity.Blogs;
import uz.itic.itic_company.example.model.mapper.BlogsMapper;
import uz.itic.itic_company.example.repository.BlogsRepository;

import java.util.UUID;
@RestController
@RequestMapping(value =  "/api/admin/blogs")
public class BlogsController  extends GenericController<Blogs, UUID, BlogsDTO, BlogsRepository, BlogsMapper> {
    public BlogsController(GenericService<Blogs, UUID, BlogsDTO, BlogsRepository, BlogsMapper> service) {
        super(service);
    }
}
