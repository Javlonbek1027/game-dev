package uz.itic.itic_company.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.itic.itic_company.example.common.basecontroller.GenericController;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.GamesDTO;
import uz.itic.itic_company.example.model.entity.Games;
import uz.itic.itic_company.example.model.mapper.GamesMapper;
import uz.itic.itic_company.example.repository.GamesRepository;

import java.util.UUID;

@RestController
@RequestMapping(value =  "/api/admin/games")
public class GamesController extends GenericController<Games, UUID, GamesDTO, GamesRepository, GamesMapper> {
    public GamesController(GenericService<Games, UUID, GamesDTO, GamesRepository, GamesMapper> service) {
        super(service);
    }
}
