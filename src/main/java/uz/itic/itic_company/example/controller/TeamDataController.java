package uz.itic.itic_company.example.controller;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.itic.itic_company.example.common.basecontroller.GenericController;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.Header;
import uz.itic.itic_company.example.model.dto.TeamDataDTO;
import uz.itic.itic_company.example.model.entity.TeamData;
import uz.itic.itic_company.example.model.exaptions.ErrorCodes;
import uz.itic.itic_company.example.model.exaptions.GeneralApiException;
import uz.itic.itic_company.example.model.mapper.TeamDataMapper;
import uz.itic.itic_company.example.repository.TeamDataRepository;

import java.util.UUID;
@RestController
@RequestMapping(value =  "/api/admin/team-data")
public class TeamDataController extends GenericController<TeamData, UUID, TeamDataDTO, TeamDataRepository, TeamDataMapper> {
    public TeamDataController(GenericService<TeamData, UUID, TeamDataDTO, TeamDataRepository, TeamDataMapper> service, TeamDataRepository teamDataRepository) {
        super(service);
        this.teamDataRepository = teamDataRepository;
    }
    private final TeamDataRepository teamDataRepository;
    @Override
    public Header<TeamDataDTO> create(Header<TeamDataDTO> dto)  {
        if(teamDataRepository.findAll().isEmpty()){
            return super.create(dto);
        }
        throw new GeneralApiException("you_do_not_create_any_more", ErrorCodes.ERROR);
    }
}
