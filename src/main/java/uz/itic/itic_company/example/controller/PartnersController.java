package uz.itic.itic_company.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.itic.itic_company.example.common.basecontroller.GenericController;
import uz.itic.itic_company.example.common.baseservice.GenericService;
import uz.itic.itic_company.example.model.dto.PartnerDto;
import uz.itic.itic_company.example.model.dto.Header;
import uz.itic.itic_company.example.model.entity.Partner;
import uz.itic.itic_company.example.model.mapper.PartnerMapper;
import uz.itic.itic_company.example.repository.PartnerRepository;
import uz.itic.itic_company.example.service.PartnersService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value =  "/api/admin/partners")
public class PartnersController extends GenericController<Partner, UUID, PartnerDto, PartnerRepository, PartnerMapper> {


    public PartnersController(GenericService<Partner, UUID, PartnerDto, PartnerRepository, PartnerMapper> service, PartnersService employeeService) {
        super(service);
    }


}
